# Klios

A spaced repetition app.

## Usage

Bootstrap it:

    fab bootstrap

Start the server:

    fab runserver

## License

MIT
